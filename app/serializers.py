from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from .models import Business, Customer, Screening, Page


class BusinessSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Business
        fields = ['store_name', 'address', 'longitude', 'latitude', 'id']

    def validate_user(self, value):
        request = self.context['request']
        return request.user


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ['name', 'content']
        lookup_field = 'name'
        extra_kwargs = {
            'url': {'lookup_field': 'name'}
        }


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ['phone_number', 'address']


class ScreeningSerializer(serializers.HyperlinkedModelSerializer):
    business = BusinessSerializer()
    customer = CustomerSerializer()
    class Meta:
        model = Screening
        fields = ['business', 'customer', 'covid_contact', 'temperature']




class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password')
        validators=[UniqueTogetherValidator(queryset=User.objects.all(), fields=['email'], message="The email is already associated with the user")]

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user