from django.contrib.auth.models import User
from rest_framework import viewsets, mixins
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from .models import Business, Customer, Screening, Page
from .serializers import BusinessSerializer, CustomerSerializer, ScreeningSerializer, UserSerializer, PageSerializer


class PageViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    """
    API endpoint that allows Pages to be viewed or edited.
    """
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    permission_classes = [permissions.AllowAny]
    lookup_field = 'name'

class CustomerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Customers to be viewed or edited.
    """
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [permissions.IsAuthenticated]


class BusinessViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Businesss to be viewed or edited.
    """
    queryset = Business.objects.all()
    serializer_class = BusinessSerializer
    permission_classes = [permissions.IsAuthenticated]


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        business = Business.objects.create(
            store_name=request.data['store_name'],
            latitude=request.data['latitude'],
            longitude=request.data['longitude'],
            address=request.data['address'],
            user=request.user)
        business = self.get_serializer(instance=business)
        return Response(business.data, status=status.HTTP_201_CREATED)

    @action(detail=True, methods=['post'])
    def scan(self, request, pk=None):
        data = request.data
        business_obj = self.get_object()
        user = business_obj.user
        if user.id != request.user.id:
            return Response({"success": False, "message": "This shop doe not belong to you"}, status=status.HTTP_401_UNAUTHORIZED)
        customer, created = User.objects.get_or_create(username=data['phone_number'])
        screening = Screening.objects.create(customer=user.customer, business=business_obj, covid_contact=data['covid_contact'], temperature=data['temperature'])

    #         business = models.ForeignKey(Business, on_delete=models.CASCADE)
    # customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    # covid_contact = models.BooleanField(default=False, help_text="have you been in contact with person with covid")
    # temperature = models.FloatField()

        # user.set_password(data['phone_number'])
        # user.save()
        serializer = ScreeningSerializer(instance=screening, context={'request': request})
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class UserCreateAPIView(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        user = User.objects.get(username=serializer.data['username'])
        token = Token.objects.create(user=user)
        return Response({"token": token.key, "data": serializer.data}, status=status.HTTP_201_CREATED, headers=headers)