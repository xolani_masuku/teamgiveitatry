from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin
from .models import Customer, Business, Screening, Page


admin.site.register(Customer)
admin.site.register(Business)
admin.site.register(Screening)


admin.site.register(Page)
