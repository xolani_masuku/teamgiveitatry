import logging
import csv 
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from app.models import Business

# Get an instance of a logger
logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def handle(self, **options):
        handle()

def handle():
    logger.info("---------------START ADDING---------------")
    business = []
    batch_size = 100
    user = User.objects.all()[0]
    with open('data.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            business.append(Business(user=user, store_name=row['Store_Name'], address=row['Full_Address'], longitude=row['Longitude'], latitude=row['Latitude']))

        #     break
        # user = models.OneToOneField(User, on_delete=models.CASCADE)
        # store_name = models.CharField(max_length=255, unique=True)
        # address = models.TextField()
        # longitude = models.CharField(max_length=255)
        # latitude = models.CharField(max_length=255)

    Business.objects.bulk_create(business, batch_size)

    logger.info("---------------FINISHED ADDING----------------")
    # print("---------------FINISHED REFRESH----------------")
