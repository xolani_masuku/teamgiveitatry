from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save, m2m_changed
from tinymce.models import HTMLField


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Customer(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=15, unique=True)
    address = models.TextField(blank=True)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def build_customer_on_user_creation(sender, instance, created, **kwargs):
  if created:
    customer = Customer(user=instance, phone_number=instance.username)
    customer.save()
    # notify_user_register(instance)


class Business(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    store_name = models.CharField(max_length=255, unique=True)
    address = models.TextField()
    longitude = models.CharField(max_length=255)
    latitude = models.CharField(max_length=255)

    def __str__(self):
        return self.store_name


class Screening(BaseModel):
    business = models.ForeignKey(Business, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    covid_contact = models.BooleanField(default=False, help_text="have you been in contact with person with covid")
    temperature = models.FloatField()

    def __str__(self):
        return self.customer.user.username


class Page(BaseModel):
    name = models.CharField(max_length=15, unique=True)
    content = HTMLField()

    def __str__(self):
        return self.name


